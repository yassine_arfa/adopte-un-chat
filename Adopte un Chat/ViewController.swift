//
//  ViewController.swift
//  Adopte un Chat
//
//  Created by Ryo on 13/02/2021.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var catImageView: UIImageView!
    
    
    var cats: [Cat] = [
        Cat(name: "Tarte Hillari", age: 6, gender: .male, imageString: "one"),
        Cat(name: "Mama", age: 4, gender: .femelle, imageString: "two"),
        Cat(name: "Marx", age: 9, gender: .male, imageString: "three"),
        Cat(name: "Bob le Roux", age: 11, gender: .male, imageString: "four"),
        Cat(name: "Princess Alizée", age: 13, gender: .femelle, imageString: "five"),
        Cat(name: "le P'tit", age: 1, gender: .male, imageString: "six"),
        Cat(name: "NoumNoum", age: 2, gender: .femelle, imageString: "seven"),
        Cat(name: "Scred", age: 5, gender: .femelle, imageString: "eight")
        
    ]
    
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundedLogo()
        containerSetup()
        catImageSetup()
        setupCat()
    }

    func roundedLogo() {
        logo.layer.cornerRadius =  logo.frame.height / 2
        logo.layer.borderColor = UIColor.systemTeal.cgColor
        logo.layer.borderWidth = 2
        
    }
    
    func containerSetup() {
        container.layer.cornerRadius = 20
        container.layer.shadowColor = UIColor.systemTeal.cgColor
        container.layer.shadowRadius = 8
        container.layer.shadowOpacity = 0.8
        container.layer.shadowOffset = CGSize(width: 2, height: 5)
    }
    
    func catImageSetup() {
        catImageView.layer.cornerRadius = 20
    }
    
    func setupCat() {
        let cat = cats[index]
        nameLabel.text = cat.name
        infoLabel.text = cat.desc
        catImageView.image = cat.setImage()
        nameLabel.textColor = cat.color
        infoLabel.textColor = cat.color
        container.layer.shadowColor = cat.cgColor
        logo.layer.borderColor = cat.cgColor
    }
    
    @IBAction func yesPressed(_ sender: Any) {
        //Au lieu de valider, continuer dans l'Array
        if index < cats.count - 1 {
            index += 1
        } else {
            index = 0
        }
        setupCat()
    }
    
    @IBAction func noPressed(_ sender: Any) {
        //Au lieu de supprimer, revenir en arrière
        if index == 0 {
            index = cats.count - 1
        } else {
            index -= 1
        }
        setupCat()
    }
}

